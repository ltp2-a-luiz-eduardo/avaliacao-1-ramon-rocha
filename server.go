package main

import("net/http"
	"time"
	"log")
	
type handler struct {}

func(Router)ServeHTTP(res http.ResponseWriter,req *http.Request) {
	ReqValidator(res, req)
}
func runservice(){	
	s := &http.Server{
	Addr:           "127.0.1.1:8080",
	Handler:        handler,
	ReadTimeout:    10 * time.Second,
	WriteTimeout:   10 * time.Second,
	MaxHeaderBytes: 1 << 20,
}
log.Fatal(s.ListenAndServe())
}
